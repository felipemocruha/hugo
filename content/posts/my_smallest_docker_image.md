+++
title = "My Smallest Docker Image, so far..."
date = "2020-05-11"
cover = ""
tags = ["golang", "docker"]
keywords = ["golang", "docker"]
description = ""
showFullContent = false
+++

## Introduction

I've been tinkering with Dockerfiles since 2016 and it became something like a hobby to hack build scripts until I get the smallest image possible.

For some stacks this can be a huge pain, like building Python using [Alpine](https://hub.docker.com/_/alpine) with musl instead of glibc. It's an endless trial and error process of installing dynamic linked libraries and sometimes It doesn't even work (e.g.  Tensorflow). Nowadays I use [debian-slim](https://hub.docker.com/_/debian) and the sizes are not that different, but it's a lot lesss painful because of glibc. Not to mention [this issue](https://pythonspeed.com/articles/alpine-docker-python/)...

In JVM languages like Scala and Clojure it's possible to create an Uberjar and just copy into the base image, but you also need to ship the JRE, so you end up with hundreds of MB as a product. You can also use [GraalVM Native Images](https://www.graalvm.org/docs/reference-manual/native-image/), but my experiences trying to make it work with Clojure were not good.

The most effective way is using languages that compile natively to ELF binaries, and even better without depending on shared libraries. You just know it'll run anywhere[1]. This is why I value so much the effort made in Go. Fast compile times plus static linked binaries is heaven for CI/CD pipelines.

## Objective

This is a sample code from [Echo](https://echo.labstack.com/), just to add some dependency and make a simple program:

```go
package main

import (
	"net/http"
	"github.com/labstack/echo/v4"
)

func main() {
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})
	e.Logger.Fatal(e.Start(":1323"))
}
```

The objective is to run this simple code inside a docker container carrying as less weight as possible.

## Building

To build a statically linked binary in Go, you can run this command:
```
CGO_ENABLED=0 go build -o example -ldflags="-w -s"
```

To understand what this command means, read the docs [here](https://golang.org/cmd/go/) or run:

```
go build --help
go build -ldflags --help
```

After this I got an executable with **6.1MB**:

```
$ ls -lh example
-rwxr-xr-x 1 felipe felipe 6.1M May 11 08:53 example
```

Pretty good, right? But this is not the smallest binary you can have. Randomly browsing on internet I found [this post](https://blog.filippo.io/shrink-your-go-binaries-with-this-one-weird-trick/). It uses [UPX](https://upx.github.io/) to compress the executable.

So, using our last built executable:

```
upx --brute example
```

This takes a little while, but it's worth! Now I have an executable with just **1.9M**:

```
ls -lh example
-rwxr-xr-x 1 felipe felipe 1.9M May 11 08:53 example
```

Now let's build a docker image using the amazing [distroless](https://github.com/GoogleContainerTools/distroless) as base image. Here is the Dockerfile:

```Dockerfile
FROM gcr.io/distroless/static

COPY example /

ENTRYPOINT ["/example"]
```

and 

```
docker build -t registry.gitlab.com/felipemocruha/go-small:v0.1.0 -f Dockerfile .
```

And the final size of the image is **3.7MB**:
```
$ docker images
REPOSITORY                                        TAG                 IMAGE ID            CREATED             SIZE
registry.gitlab.com/felipemocruha/go-small        v0.1.0              a98df1c16870        2 hours ago         3.7MB
```

Let's run the container:

```
$ docker run --network=host registry.gitlab.com/felipemocruha/go-small:v0.1.0

   ____    __
  / __/___/ /  ___
 / _// __/ _ \/ _ \
/___/\__/_//_/\___/ v4.1.16
High performance, minimalist Go web framework
https://echo.labstack.com
____________________________________O/_______
                                    O\
⇨ http server started on [::]:1323
```

Success!

## Conclusion

I pushed this to my gitlab registry, the final compressed image had **2.43MiB**. This is really impressive!
Combining this with a very fast startup from Go, a Kubernetes cluster can pull this image and make a deploy in just a few seconds. I think this is the major upside from using Go.  

I could probably squeeze it a little more using `FROM SCRATCH` but this is good enough for now, and can be used in production.

Thanks for reading.

### Notes

[1] Anywhere\* considering the same CPU architecture and OS targets...

+++
title = "Trying to run Apache Beam with Clojure"
date = "2020-04-27"
cover = ""
tags = ["clojure", "apache beam"]
keywords = ["clojure", "apache beam"]
description = "My failed attempt to write a simple pipeline in clojure"
showFullContent = false
+++

I had the brilliant idea to replicate the [WordCount](https://beam.apache.org/get-started/quickstart-java/) example from Apache Beam's Getting Started page, after some time it looked like this:

```clojure
(ns wordcount
  (:import (java.util Arrays)
           (org.apache.beam.sdk Pipeline)
           (org.apache.beam.sdk.io TextIO)
           (org.apache.beam.sdk.options PipelineOptions
                                        PipelineOptionsFactory)
           (org.apache.beam.sdk.transforms Count
                                           Filter
                                           FlatMapElements
                                           MapElements)
           (org.apache.beam.sdk.values TypeDescriptors))
  (:gen-class))

;(set! *warn-on-reflection* true)

(defn line->arrays [^String line]
    (-> (.split line "[^\\p{L}]+")
        (.asList Arrays)))
    
(defn make-printable-string [word-count]
  (str (.getKey word-count) ": " (.getValue word-count)))

(def strs-desc (.. TypeDescriptors strings))

(defn -main [& args]
  (let [pipeline (Pipeline/create (PipelineOptionsFactory/create))]

    (-> pipeline
        (.apply (.from (TextIO/read) "lorem"))
        (.apply (.via (FlatMapElements/into strs-desc)) line->arrays)
        (.apply (Filter/by empty?))
        (.apply (Count/perElement))
        (.apply (.via (MapElements/into strs-desc) make-printable-string))
        (.apply (.to (TextIO/write) "out/wordcounts")))

    (.waitUntilFinish (.run pipeline))))
```

It would be reeeeeeeeally nice if this worked, but it doesn't :(

Trying to run raises this error:

```
SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
Execution error (IllegalArgumentException) at wordcount/-main (wordcount.clj:30).
No matching field found: via for class org.apache.beam.sdk.transforms.FlatMapElements
```

I guess it's because the reflector cannot infer the underlying types right. I have tried multiple type annotations after reading [this answer](https://clojureverse.org/t/weird-java-interop-issues/4395/17) on clojureverse, but no success either.

After this, I realized that Java's lambdas can be different from clojure's functions, and my `line->arrays` doesn't implement one of Beam's transform functions like [ProcessFunction](https://beam.apache.org/releases/javadoc/2.20.0/org/apache/beam/sdk/transforms/ProcessFunction.html). 

I tried to implement this interface with `deftype` and `reify` but it doesn't worked either...

After this I gave up, looks like a waste of time even if reify worked, it would need a lot of boilerplate to make real pipelines. a lot o PTransforms take lambdas as parameters...

[Here is the repo with the code, BTW](https://gitlab.com/felipemocruha/wordcount)

Nothing like a failure to my very first blog post. Welcome!
